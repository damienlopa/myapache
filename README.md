# Myapache

## Usage
```bash
python myapache -h
usage: myapache.py [-h] [--port PORT] [--rootdir ROOTDIR]

optional arguments:
  -h, --help         show this help message and exit
  --port PORT
  --rootdir ROOTDIR
```

```bash
python myapache --port 7070
```

```bash
python myapache --rootdir /
```