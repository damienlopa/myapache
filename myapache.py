from datetime import datetime
import os
import socketserver
import mimetypes
import argparse
from pathlib import Path


class TCPSocketHandler(socketserver.BaseRequestHandler):

    def header(self, mime_type):
        """
            Add request header with status code and content-type
        """
        self.render_data("HTTP/1.0 200 OK\n")
        self.render_data('Content-Type: ' + mime_type + '\n')
        self.render_data('\r\n')

    def get_size(self, item):
        """
            Get the size of folder or file
        """
        try:
            size = os.path.getsize(f'{self.server.rootdir}{self.path}{item}')
        except OSError:
            size = 0
        return size

    def get_last_modification_date(self, item):
        """
            Get last modification date of folder and file
        """
        try:
            path = f'{self.server.rootdir}{self.path}{item}'
            last_time_edit = os.path.getmtime(path)
            dt_object = datetime.fromtimestamp(last_time_edit)
            dt_string = dt_object.strftime("%d-%b-%Y %H:%M:%S")
        except OSError:
            dt_string = ""
        return dt_string

    def render_data(self, data):
        """
            Encode and send passed data
        """
        self.request.send(str.encode(data))

    def render_tab_line(self, name, icon, path, size, date):
        """
            Render a tab line
        """
        icon_html = f'<td valign="top"><img src="{icon}" width="15"></td>'
        path_html = f'<td><a href="{path}">{name}</a></td>'
        date_html = f'<td align="right">{date}</td>'
        size_html = f'<td align="right">{size}</td>'
        html = f'<tr>{icon_html}{path_html}{date_html}{size_html}</tr>'
        self.render_data(html)

    def render_previous_parent(self):
        """
            Render the previous path
        """
        previous_path = Path(self.path).resolve().parents
        if previous_path:
            previous_path = previous_path[0]
            self.render_tab_line("Parent directory",
                                 "/assets/back.gif",
                                 previous_path,
                                 "",
                                 "")

    def render_folders(self, folders):
        """
            Render folder line
        """
        for folder in folders:
            dt_string = self.get_last_modification_date(folder)
            size = self.get_size(folder)
            self.render_tab_line(folder,
                                 "/assets/folder.gif",
                                 f'{self.path}{folder}',
                                 size,
                                 dt_string)

    def render_files(self, files):
        """
            Render file line
        """
        for file in files:
            dt_string = self.get_last_modification_date(file)
            size = self.get_size(file)
            self.render_tab_line(file,
                                 "/assets/unknown.gif",
                                 f'{self.path}{file}',
                                 size,
                                 dt_string)

    def get_path(self, request):
        """
            Get path from request
        """
        lines = request.splitlines()
        self.path = ""
        path_line = str(lines[0])
        if "GET" in path_line:
            path = path_line.split(" ")[1]
            self.path = path
        else:
            return None

    def clean_path(self):
        """
            Clean path to avoid double /
        """
        if self.path != "/" and self.path[:-1] != "/":
            self.path += "/"
        self.path.replace("//", "/")

    def get_current_folders_files(self):
        """
            Stock files and folders from current
            directory
        """
        files = []
        folders = []

        path_to_open = self.server.rootdir + self.path
        tree = os.listdir(path_to_open)

        for item in tree:
            if os.path.isfile(os.path.join(path_to_open, item)):
                files.append(item)
            if os.path.isdir(os.path.join(path_to_open, item)):
                folders.append(item)
        return folders, files

    def open_file(self, path_to_open):
        """
            Get mime type and open it
        """
        mime_type = mimetypes.MimeTypes().guess_type(path_to_open)[0]
        if mime_type:
            self.header(mime_type)
            f = open(path_to_open, 'rb')
            self.request.send(f.read())
        else:
            # TODO: Add download feature instead of 404
            self.render_data("HTTP/1.0 404 Not Found\n")

    def list_folders_files(self, path_to_open):
        """
            List all files and folders from current repository
        """
        folders, files = self.get_current_folders_files()

        self.header("text/html")

        self.clean_path()
        self.render_data(f"<h1>Index of {self.path}</h1><br>")
        self.render_data("<table><tr>")
        self.render_data('<th></th><th><a>Name</a></th>'
                         '<th><a>Last modified</a></th>'
                         '<th align="right"><a>Size</a></th></tr>'
                         '<tr><th colspan="5"><hr></th></tr>')

        self.render_previous_parent()
        self.render_folders(folders)
        self.render_files(files)

        self.render_data("</tr></table>")

    def handle(self):
        """
            Retrieve request and dispatch
        """
        self.data = self.request.recv(1024).strip()

        self.get_path(self.data)

        try:
            if self.path != "":
                path_to_open = self.server.rootdir + self.path
                path_to_open.replace("//", "/")
                if os.path.isfile(path_to_open):
                    self.open_file(path_to_open)
                else:
                    self.list_folders_files(path_to_open)
            else:
                self.render_data("HTTP/1.0 404 Not Found\n")
        except FileNotFoundError:
            self.render_data("HTTP/1.0 404 Not Found\n")


def check_port(value):
    if int(value) < 0 or int(value) > 65535:
        raise argparse.ArgumentTypeError("port must be 0-65535")
    return value


def check_rootdir(value):
    if not os.path.exists(value):
        raise argparse.ArgumentTypeError("invalid path")
    return value


if __name__ == "__main__":
    try:
        parser = argparse.ArgumentParser()
        parser.add_argument('--port', default=9090, type=check_port)
        parser.add_argument('--rootdir', default="./", type=check_rootdir)
        params = parser.parse_args()

        print(f"Server running at 0.0.0.0:{params.port}\n(Press Ctrl to quit)")
        my_server = socketserver.TCPServer(("localhost", int(params.port)),
                                           TCPSocketHandler)
        my_server.rootdir = params.rootdir
        my_server.serve_forever()

    except KeyboardInterrupt:
        print("\nBye !")
        my_server.shutdown()
        my_server.server_close()
